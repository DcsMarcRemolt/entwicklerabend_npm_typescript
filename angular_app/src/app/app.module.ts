import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MyAwesomeLibModule } from 'my-awesome-lib';

import { AppComponent }  from './app.component';


@NgModule({
  imports:      [
    BrowserModule,
    MyAwesomeLibModule
  ],
  declarations: [ AppComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }

