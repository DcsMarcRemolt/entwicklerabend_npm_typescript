import { Component } from '@angular/core';
import { List } from 'immutable';
import { listToString } from 'my-awesome-lib';

@Component({
  selector: 'my-app',
  template: `
    <h1>Hello {{ peopleList }}!</h1>
    <dcs-answer answer="42" [people]="people"></dcs-answer>
  `
})
export class AppComponent {

  get peopleList(): string {
    return listToString(this.people);
  }

  people: List<string> = List([
    'Arthur',
    'Trillian',
    'Ford'
  ]);

}
