import { List } from 'immutable';

export function listToString(items: List<string>): string {
  return items.join(', ');
}
