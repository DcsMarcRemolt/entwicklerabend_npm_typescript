import { expect } from 'chai';

import { listToString } from './helpers';
import { List } from 'immutable';


describe('helpers', () => {

  describe('listToString', () => {

    it('joins a Immutable List', () => {
      expect(listToString(List(['one', 'two', 'three']))).to.equal('one, two, three');
    });

  });

});
