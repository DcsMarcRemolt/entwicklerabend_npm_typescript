import { Component, Input } from '@angular/core';
import { List } from 'immutable';

@Component({
  selector: 'dcs-answer',
  template: `
    <p>Hello {{ firstPerson }}, the answer is {{ answer }}!!!</p>
    <ul>
      <li *ngFor="let person of people">{{ person }}</li>
    </ul>
  `
})
export class AnswerComponent {
  @Input() answer: number;
  @Input() people: List<string>;

  get firstPerson(): string {
    return this.people ? this.people.first() : '';
  }

  get secondPerson(): string {
    return this.people.get(1);
  }
}
