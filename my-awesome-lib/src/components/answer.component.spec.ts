import { AnswerComponent } from './answer.component';
import { List } from 'immutable';
import { expect } from 'chai';


describe('AnswerComponent', () => {

  let subject: AnswerComponent;

  describe('firstPerson', () => {

    beforeEach(() => {
      subject = new AnswerComponent();
      subject.people = List([
        'Axel',
        'Chris',
        'Don',
        'Florian',
        'Frank',
        'Kristian',
        'Matthias',
        'Peter',
        'Wowo'
      ])
    })

    it('returns the first person from people List', () => {
      expect(subject.firstPerson).to.equal('Axel');
    });
  })

});
