import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AnswerComponent } from './components/answer.component';


@NgModule({
  declarations: [
    AnswerComponent
  ],
  exports: [
    AnswerComponent
  ],
  imports: [
    CommonModule
  ]
})
export class MyAwesomeLibModule { }
