require('jsdom-global')(undefined, {
  url: 'http://localhost'
})

require('ts-node').register({
  lazy: false,
  fast: true,
  compilerOptions: {
    module: 'commonjs',
    noEmitHelpers: false
  }
});

require('zone.js/dist/long-stack-trace-zone');
require('zone.js/dist/async-test');
require('zone.js/dist/fake-async-test');
require('zone.js/dist/sync-test');
require('zone.js/dist/proxy'); // since zone.js 0.6.14

var chai = require('chai');
var chaiImmutable = require('chai-immutable');
chai.use(chaiImmutable);

window.Reflect = Reflect;

var testing = require('@angular/core/testing');
var browser = require('@angular/platform-browser-dynamic/testing');

testing.TestBed.initTestEnvironment(
  browser.BrowserDynamicTestingModule,
  browser.platformBrowserDynamicTesting()
);
